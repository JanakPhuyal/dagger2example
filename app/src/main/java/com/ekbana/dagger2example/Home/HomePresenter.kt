package com.ekbana.dagger2example.Home

import com.ekbana.dagger2example.model.Movies
import com.ekbana.dagger2example.model.MoviesList
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import javax.inject.Inject

class HomePresenter @Inject constructor(private val homeInteractor: HomeInteractor) : MvpBasePresenter<HomeView>() {
    fun getMoviesList() {
        homeInteractor.getMoviesList().subscribe({ demo ->
            ifViewAttached { view ->
                view.setMoviesAdapter(demo.results as MutableList<Movies>)

            }
        }, { error ->

        })
    }

}