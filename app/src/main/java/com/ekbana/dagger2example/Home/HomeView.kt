package com.ekbana.dagger2example.Home

import com.ekbana.dagger2example.model.Movies
import com.hannesdorfmann.mosby3.mvp.MvpView

interface HomeView : MvpView {
    fun setMoviesAdapter(moviesList: MutableList<Movies>)

}