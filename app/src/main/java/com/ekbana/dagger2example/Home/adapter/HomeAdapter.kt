package com.ekbana.dagger2example.Home.adapter


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.ekbana.dagger2example.Home.ViewHolder.HomeVH
import com.ekbana.dagger2example.R
import com.ekbana.dagger2example.model.Movies
import com.ekbana.dagger2example.model.MoviesList
import com.squareup.picasso.Picasso

class HomeAdapter(var list: MutableList<Movies>) : RecyclerView.Adapter<HomeVH>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): HomeVH {
        var mainView = LayoutInflater.from(parent?.context).inflate(R.layout.home_adapter, parent, false)
        return HomeVH(mainView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: HomeVH?, position: Int) {
        var movies = list.get(position)
        holder?.txvMovieReleaseDate?.text = movies.releaseDate
        holder?.txvMovieTitle?.text = movies.title
        Picasso.get()
                .load("http://image.tmdb.org/t/p/w185" + movies.posterPath)
                .placeholder(R.drawable.sanju_poster)
                .error(R.drawable.sanju_poster)
                .into(holder?.iavMoviePoster);

    }
}