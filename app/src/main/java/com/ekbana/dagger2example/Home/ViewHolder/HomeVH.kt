package com.ekbana.dagger2example.Home.ViewHolder

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.home_adapter.view.*


class HomeVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txvMovieTitle = itemView.txvMovieTitle
    var txvMovieReleaseDate = itemView.txvMovieReleaseDate
    var iavMoviePoster = itemView.iavMoviePoster
}