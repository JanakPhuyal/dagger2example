package com.ekbana.dagger2example.Home

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.ekbana.dagger2example.Home.adapter.HomeAdapter
import com.ekbana.dagger2example.R
import com.ekbana.dagger2example.component.DaggerMainComponent
import com.ekbana.dagger2example.component.MainComponent
import com.ekbana.dagger2example.model.Movies
import com.ekbana.dagger2example.module.DaggerModule

import com.hannesdorfmann.mosby3.mvp.MvpActivity
import kotlinx.android.synthetic.main.home_layout.*
import javax.inject.Inject

class HomeActivity : MvpActivity<HomeView, HomePresenter>(), HomeView {

    @Inject
    lateinit var homePresenter: HomePresenter
    lateinit var dagger2Component: MainComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        initDaggerInjection()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_layout)
        initRecyclerView()
        presenter.getMoviesList()


    }

    private fun initDaggerInjection() {
        dagger2Component = DaggerMainComponent.builder().daggerModule(DaggerModule()).build()
        dagger2Component.inject(this)
    }

    override fun setMoviesAdapter(moviesList: MutableList<Movies>) {
        recyclerHome.adapter = HomeAdapter(moviesList )
    }

    fun initRecyclerView() {
        recyclerHome.layoutManager = LinearLayoutManager(this)
    }

    override fun createPresenter() = homePresenter
}