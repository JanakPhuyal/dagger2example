package com.ekbana.dagger2example.Home

import com.ekbana.dagger2example.model.Movies
import com.ekbana.dagger2example.model.MoviesList
import com.ekbana.dagger2example.repository.ApiRepository
import io.reactivex.Single
import javax.inject.Inject

class HomeInteractor @Inject constructor(private var apiRepository: ApiRepository) {

    fun getMoviesList() : Single<MoviesList> = apiRepository.getMoviesList()


}