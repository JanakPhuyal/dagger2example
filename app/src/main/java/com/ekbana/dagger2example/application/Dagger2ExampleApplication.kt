package com.ekbana.dagger2example.application

import android.app.Application
import com.ekbana.dagger2example.component.MainComponent
import com.ekbana.dagger2example.module.DaggerModule

class Dagger2ExampleApplication : Application() {
    lateinit var dagger2Component: MainComponent
    override fun onCreate() {
        super.onCreate()
    }

}