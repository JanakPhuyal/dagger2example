package com.ekbana.dagger2example.repository

import com.ekbana.dagger2example.model.Movies
import com.ekbana.dagger2example.model.MoviesList
import com.ekbana.dagger2example.network.ApiService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ApiRepository @Inject constructor(var apiService: ApiService) {
    fun getMoviesList(): Single<MoviesList> =
            Single.create({ e ->
                apiService.getMoviesList("ef8f48b43832a9e95b87408bf739ed51").subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ response ->
                            if (response.isSuccessful) {
                                e.onSuccess(response.body()!!)
                            } else
                                e.onError(Throwable("Error API call"))
                        }, { error ->
                            e.onError(error)
                        })
            })
}