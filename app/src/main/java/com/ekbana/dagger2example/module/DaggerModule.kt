package com.ekbana.dagger2example.module

import com.ekbana.dagger2example.network.ApiService
import com.ekbana.dagger2example.network.RetrofitHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DaggerModule {
    @Singleton
    @Provides
    fun providesApiService(): ApiService = RetrofitHelper.getApiService()

}