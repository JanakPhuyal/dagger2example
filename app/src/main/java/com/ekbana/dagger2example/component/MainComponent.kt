package com.ekbana.dagger2example.component

import com.ekbana.dagger2example.Home.HomeActivity
import com.ekbana.dagger2example.MainActivity
import com.ekbana.dagger2example.module.DaggerModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DaggerModule::class])
interface MainComponent {
    fun inject(mainActivity: HomeActivity)
}