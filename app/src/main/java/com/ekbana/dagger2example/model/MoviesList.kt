package com.ekbana.dagger2example.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import android.media.Rating


class MoviesList {
    @SerializedName("page")
    @Expose
     val page: Int? = null
    @SerializedName("total_results")
    @Expose
   val totalResults: Int? = null
    @SerializedName("total_pages")
    @Expose
    val totalPages: Int? = null
    @SerializedName("results")
    @Expose
  val results: List<Movies>? = null

}