package com.ekbana.dagger2example.network

import com.ekbana.dagger2example.model.Movies
import com.ekbana.dagger2example.model.MoviesList
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface ApiService {
    @GET("top_rated/")
    fun getMoviesList( @Query("api_key") apiKey: String): Single<Response<MoviesList>>
}